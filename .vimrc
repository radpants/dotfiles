
set cursorline
set tabstop=4
set autoindent
set nu
set background=dark
set softtabstop=4
set shiftwidth=4
set noexpandtab
set scrolloff=4

call plug#begin("~/.vim/plugged")

Plug 'morhetz/gruvbox'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'tpope/vim-fugitive'
Plug 'majutsushi/tagbar'
Plug 'vim-syntastic/syntastic'
Plug 'flazz/vim-colorschemes'

call plug#end()

colors gruvbox

let g:airline_theme = 'gruvbox'
let g:syntastic_cpp_include_dirs = [ '/usr/local/include' ]
let g:syntastic_c_include_dirs = [ '/usr/local/include' ]

nmap <C-t> :TagbarToggle<CR><C-w><C-w>

function TrimWhitespace()
  %s/\s*$//
  ''
:endfunction
command! Trim call TrimWhitespace()
